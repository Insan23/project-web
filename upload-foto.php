<?php
$target_dir = "foto/";
$target_file = $target_dir . 'profil.jpg';
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["simpan"])) {
	$check = getimagesize($_FILES["foto-profil"]["tmp_name"]);
	if($check !== false) {
		//echo "File is an image - " . $check["mime"] . ".";
		$uploadOk = 1;
	} else {
		//echo "File is not an image.";
		$uploadOk = 0;
	}
	if ($_FILES["foto-profil"]["size"] > 500000) {
		//echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		//echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	$uploadOk = 0;
	}

	$error = "Gagal Upload Foto Profil";
	$success = "Berhasil Upload Foto";
	if ($uploadOk == 0) {
		include 'biodata.php';
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["foto-profil"]["tmp_name"], $target_file)) {
			include 'biodata.php';
		} else {
			include 'biodata.php';
		}
	}
}
?>